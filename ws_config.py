#!/usr/bin/python3
import os
from ws_keys import KeyRepeater, KeyHolder, exit_key

def load_user_config(delay=0.1):
    global exit_key
    config_loc = os.path.join(os.path.expanduser('~'), '.config', 'wrist-saver', 'wsrc')
    keys = dict()

    with open(config_loc) as f:
        content = f.readlines()
    content = [x.strip() for x in content] 
    for line in content:
        # Check for comment
        if len(line) > 0 and line[0] != '#':
            line = line.split(' ')
            try:
                type, key_pressed, key_sent = line
            except ValueError:
                type, key_pressed = line
            if type == 'r:':
                keys[key_pressed] = KeyRepeater(key_sent, delay)
                keys[key_pressed].start()
            if type == 'h:':
                keys[key_pressed] = KeyHolder(key_sent)
                keys[key_pressed].start()
            if type == 'e:':
                exit_key = key_pressed
    return keys

