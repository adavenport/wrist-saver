import time
import threading
from pynput.keyboard import Controller

keyboard = Controller()
exit_key = 'undefined'

def raw_key(key):
    return str(key).replace('Key.', '')

class KeyManager(threading.Thread):
    def __init__(self):
        super(KeyManager, self).__init__()
        self.program_running = True

    def press(self):
        pass

    def release(self):
        pass

    def exit(self):
        self.release()
        self.program_running = False


class KeyRepeater(KeyManager):
    def __init__(self, button, delay):
        super(KeyRepeater, self).__init__()
        self.delay = delay
        self.button = button
        self.running = False

    def press(self):
        global keyboard
        keyboard.press(self.button)
        keyboard.release(self.button)
        self.running = True

    def release(self):
        self.running = False

    def run(self):
        global keyboard
        while self.program_running:
            while self.running:
                keyboard.press(self.button)
                time.sleep(self.delay) / 2
                keyboard.release(self.button)
                time.sleep(self.delay) / 2
            time.sleep(0.1)


class KeyHolder(KeyManager):
    def __init__(self, button):
        super(KeyHolder, self).__init__()
        self.button = button
        self.held = False

    def press(self):
        global keyboard
        if self.held:
            keyboard.release(self.button)
        else:
            keyboard.press(self.button)

