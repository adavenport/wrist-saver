# Wrist Saver #
This utility is primarily aimed towards games like mmos that require pressing the same key repetitively.
Be advised that this tool is still very early in development.



## Requirements ##

* Python 3
* pynput - https://pypi.org/project/pynput/


## Usage ##
The configuration file is stored at ~/.config/wrist-saver/wsrc

A sample configuration file (wsrc) is included.

~~~
r: f13 1
h: f2 w
~~~

### Key Types ###
r: repeat
h: hold

### Keys ####
"f13" is the key you are pressing and "1" is the key you want to send.
You can change or add to this to suit your needs.
There is currently no limit on how many keys you can setup so be careful (remember this is python).
Once you have your configuration set you can run wrist-saver.py in the background, you may wish to set it to run at startup after some testing.



## In Progress ##

* Support different modes (repeat, hold, etc.)


## Future Plans ##

* This was originally created in Python due to how quick I was able to get a working prototype.
* I am planning on eventually porting this app to C++ to improve performance.
* I may also use qt for easier user configuration.


## Acknowledgements ##
This project was made possible due to the hard work of Noah Metz on his Orbweaver remapper: https://gitlab.com/Xelynega/orbweaver-key-remapper
I am now able to make use of this keypad on Linux.
