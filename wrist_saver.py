#!/usr/bin/python3
from pynput.keyboard import Listener
from ws_config import load_user_config
keys = load_user_config()
from ws_keys import exit_key, raw_key


def on_press(key):
    global keys
    key = raw_key(key)
    if key in keys:
        keys[key].press()
    elif key == exit_key:
        # Todo - make exit more graceful
        os._exit(1)

def on_release(key):
    global keys
    key = raw_key(key)
    if key in keys:
        keys[key].release()

with Listener(on_press=on_press,
              on_release=on_release) as listener:
    listener.join()
